import csv
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from MyTableClass import Airport
import requests


engine = create_engine("mysql://root:pass@127.0.0.1:3306/", echo=False)
engine.execute("create database if not exists mydb")
engine = create_engine("mysql://root:pass@127.0.0.1:3306/mydb?charset=utf8mb4")

if not sqlalchemy.inspect(engine).has_table('airports'):
    Airport.metadata.create_all(engine)
else:
    engine.execute((Airport.__table__.delete()))
Session = sessionmaker(bind=engine)
session = Session()

csv_url = "https://datahub.io/core/airport-codes/r/airport-codes.csv"
csv_file = requests.request("GET", csv_url).text.split('\n')
csv_file = csv.DictReader(csv_file)

for row in csv_file:
    longitude, latitude = row['coordinates'].split(', ')
    session.add(Airport(row['ident'], row['iata_code'], row['name'], row['iso_region'], latitude, longitude))
session.commit()











