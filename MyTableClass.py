from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Unicode


Base = declarative_base()


class Airport(Base):
    __tablename__ = 'airports'
    id = Column(String(10), primary_key=True)
    code = Column(String(10))
    name = Column(Unicode(100, collation="utf8mb4_bin"))
    country = Column(String(10))
    latitude = Column(String(30))
    longitude = Column(String(30))

    def __init__(self, new_id, new_code, new_name, new_country, new_longitude, new_latitude):
        self.id = new_id
        self.code = new_code
        self.name = new_name
        self.country = new_country
        self.latitude = new_latitude
        self.longitude = new_longitude

    def __repr__(self):
        return "<Airport(id: '%s' 'code: %s'  'name: %s' 'country: %s' 'coordinates: %s %s')>" \
               % (self.id, self.code, self.name, self.country, self.latitude, self.longitude)
