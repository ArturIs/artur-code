from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from MyTableClass import Airport
from sys import argv

engine = create_engine("mysql://root:pass@127.0.0.1:3306/mydb?charset=utf8mb4")
Session = sessionmaker(bind=engine)
session = Session()

query = argv[1]
response = session.query(Airport).filter_by(code=query)

if response.first() is None:
    print(f"There isn't an airport with code {query}")
else:
    for airport in response:
        print(airport)
