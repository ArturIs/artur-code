#!/bin/bash

sudo apt-get install libmysqlclient-dev python3-dev
pyenv install $(cat .python-version)
pip install -r requirements.txt


docker run \
--name=mysql \
--env="MYSQL_ROOT_HOST=%" \
--env="MYSQL_ROOT_USERNAME=root" \
--env="MYSQL_ROOT_PASSWORD=pass" \
-v "$(pwd)/data:/var/lib/mysql" \
-p 3306:3306 \
--rm \
--detach \
mysql:5.7 mysqld &

while ! mysqladmin ping -h"127.0.0.1" --silent; do
    sleep 1
done

python ./main.py
python ./search.py ZIA
python ./distance.py ZIA DME
sleep 10
docker stop mysql