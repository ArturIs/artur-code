from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from MyTableClass import Airport
from sys import argv
import requests

engine = create_engine('mysql://root:pass@127.0.0.1:3306/mydb?charset=utf8mb4')
Session = sessionmaker(bind=engine)
session = Session()

query = [argv[1], argv[2]]
response = session.query(Airport).filter(Airport.code.in_(query))

coordinates = []
for my_code in query:
    airport = response.filter_by(code=my_code).first()
    if airport is None:
        print(f"There is no airport with code '{my_code}'\nDistance between airports can't be calculated")
        break
    else:
        print(airport)
        coordinates.append("{},{}".format(airport.latitude, airport.longitude))

api_key = "H_OKca5zrFlPtArat0_ZcLEwUKjCn_dxO-MDBwAeCvQ"
coordinates = "{}:{}".format(coordinates[0], coordinates[1])
querystring = "https://atlas.microsoft.com/spatial/greatCircleDistance/json?" \
              "subscription-key={}&api-version=1.0&query={}".format(api_key, coordinates)
#print(querystring)
response = requests.request("GET", querystring).json()
print(response['result'])

